class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presence_of :tite
  validates_presence_of :body
end
